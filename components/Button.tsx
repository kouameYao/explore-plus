import Image from "next/image";

type ButtonProps = {
  type: "button" | "submit";
  title: string;
  icon?: string;
  full?: boolean;
  variant:
    | "btn_green"
    | "btn_dark_green"
    | "btn_dark_green_outline"
    | "btn_white"
    | "btn_white_text"
    | "btn_dark_blue"
    | "btn_light_blue";
};

export const Button = ({ type, title, icon, full, variant }: ButtonProps) => {
  return (
    <button
      type={type}
      className={`flexCenter gap-3 rounded-full border ${variant} ${
        full && "w-full"
      }`}
    >
      {icon && <Image src={icon} alt="title" width={24} height={24} />}
      <label className="bold-16 whitespace-nowrap"> {title} </label>
    </button>
  );
};
